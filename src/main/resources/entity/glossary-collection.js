define('conf-glossary/entity/glossary-collection', [
    'conf-glossary/entity/term',
    'conf-glossary/lib/backbone',
    'conf-glossary/lib/underscore',
    'jquery'
], function(Term, Backbone, _, $) {

    var GlossaryCollection = Backbone.Collection.extend({
        model: Term,

        persist: function() {
            var self = this;
            var promises = this.chain()
                .filter(function(model) {
                    return model.isNew() || model.hasChanged();
                }).each(function(model) {
                    return model.save();
                }).value();
            $.when.apply($, promises).then(function(r) {
                self.trigger('persisted', r);
            });
        },

        findByTerm: function findByTerm(termName) {
            if (!termName) return false;
            var search = termName.toLowerCase();
            return this.find(function(model) {
                return model.slug == search || model.get('name').toLowerCase() == search;
            });
        }
    });

    return GlossaryCollection;
});
