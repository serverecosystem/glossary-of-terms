/**
 * TODO: Should be custom element, or inline dialog.
 * TODO: Should get created by (and instance owned by) the term element itself.
 */
define('conf-glossary/feature/term-dialog/term-dialog', ['require'], function(require) {
    var Meta = AJS.Meta;
    var glossary = require('conf-glossary/feature/glossary/glossary');
    var _ = require('conf-glossary/lib/underscore');
    var $ = require('jquery');

    var $noTerm = $("<div></div>");
    var space = Meta.get('space-key');

    function getTermByName(name) {
        return glossary.findByTerm(name);
    }

    var dfnTemplate = _.template('<li <% if (data.scope) { %>data-scope="<%= data.scope %>"<% } %>><p><%= data.content %><% if (data.url && data.url.length) { %> &mdash; <a href="<%= data.url %>">more...</a><% } %></p></li>', undefined, {variable:"data"});
    var popupElement = _.template('<div class="glossary-term-hover" id="<%= term.id %>"></div>');
    var popupContents = _.template(''
        +'<h3><%= term.name %></h3>'
        +'<% if (term.grouped[space] && term.grouped[space].length) { %>'
        +'<ul class="definitions this-space"><% _.each(term.grouped[space], function(dfn) { %><%= dfnTemplate(dfn) %><% }); %></ul>'
        +'<% } %>'
        +'<ul class="definitions everywhere"><% _.each(term.grouped["_"], function(dfn) { %><%= dfnTemplate(dfn) %><% }); %></ul>'
        +'');

    function createTermPopup(term) {
        var id = term.domId();
        var $el = $(document.getElementById(id));
        var data = _.clone(term.attributes);
        data.id = id;
        data.grouped = _.groupBy(term.definitions(), function(dfn) { return (dfn.scope) ? dfn.scope : "_"; });
        var html = popupContents({ term: data, space: space, dfnTemplate: dfnTemplate });
        if (!$el.length) {
            $el = $(popupElement({term: data})).html(html);
            $el.bind("mouseenter", function(e) { onEnterPopup($el); });
            $el.bind("mouseleave", function(e) { onLeavePopup($el); });
        } else {
            $el.html(html);
        }
        $el.appendTo(document.body);
        return $el;
    }

    function getTermPopup(termName) {
        var term = getTermByName(termName);
        if (!term) return $noTerm;
        term.popup = createTermPopup(term);
        return term.popup;
    }

    function onEnterPopup(popup) {
        var $popup = (popup.jquery) ? popup : $(popup);
        clearTimeout($popup.data("closing"));
        $popup.data("closing", null);
    }

    function onLeavePopup(popup) {
        var $popup = (popup.jquery) ? popup : $(popup);
        clearTimeout($popup.data("closing"));
        $popup.data("closing", setTimeout(function() { hidePopup($popup) }, 1000));
    }

    function hidePopup($popup) {
        if ($popup && $popup.data("closing")) {
            $popup.hide("slow");
        }
    }

    function showTermPopup(element, e) {
        var $el = $(element);
        var termName = $el.attr("title");
        var $popup = getTermPopup(termName);
        $popup.hide();
        $popup.css({
            "position": "fixed",
            "top": e.clientY,
            "left": e.clientX
        });
        $popup.show();
        onEnterPopup($popup);
    }

    function closeTermPopup(element) {
        var $el = $(element);
        var termName = $el.attr("title");
        var $popup = getTermPopup(termName);
        onLeavePopup($popup);
    }

    function hideAllPopupsRightNow() {
        $(".glossary-term-hover").each(function() {
            var $popup = $(this);
            clearTimeout($popup.data("closing"));
            $popup.hide();
        });
    }


    return {
        showTermPopup: showTermPopup,
        closeTermPopup: closeTermPopup,
        hideAll: hideAllPopupsRightNow
    }
});
