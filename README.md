# Glossary of Terms plugin

This repository is be the base for demonstrating how to build a more complicated front-end for your Atlassian add-on.

It is extended and adjusted by other repositories and tutorials to demonstrate various ways to build up your front-end,
as well as various features of the add-on system.

## Getting started 

To build or run this add-on, you'll need to install Java and the Atlassian SDK. You can learn how to [set up the Atlassian SDK and build a project][2] on [developer.atlassian.com][1].

### Building the add-on

Once you have the Atlassian SDK installed and working, the only things you should have to do are: 

1. Open a terminal window or command prompt
2. Navigate to the checkout directory for this repository
2. Call `atlas-mvn package`

### See it in action

Once the add-on is built -- or indeed, to build the add-on and then run it -- you can call `atlas-run`.

## Tutorials using this add-on

A number of tutorials use this add-on's code as a starting point to demonstrate APIs, techniques and tricks for building Server add-ons. Most have forked the repository, so you can check the list of forks for this repo to find them. Here are a few noteworthy tutorials:

* [Compiling a Server add-on's JavaScript using Babel and Gulp][301]
* [Bundling the UI for a Server add-on using Webpack][302]

[1]: https://developer.atlassian.com
[2]: https://developer.atlassian.com/docs/getting-started/set-up-the-atlassian-plugin-sdk-and-build-a-project
[301]: https://bitbucket.org/serverecosystem/sao4fed-compile-the-ui
[302]: https://bitbucket.org/serverecosystem/sao4fed-bundle-the-ui