(function() {
    'use strict';
    var idb = window.idb;
    delete window.idb;

    define('conf-glossary/lib/idb', function() {
        return idb;
    });
})();
