(function() {
    'use strict';
    var ES6Promise = window.ES6Promise;
    delete window.ES6Promise;

    define('conf-glossary/lib/promise', function() {
        return ES6Promise;
    });

    // Put promise in window, so we don't need to modify the libs we're pulling in
    // that expect it to be there.
    if (!('Promise' in window)) {
        ES6Promise.polyfill();
    }
})();
