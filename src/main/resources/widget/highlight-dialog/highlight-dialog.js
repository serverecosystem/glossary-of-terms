/* global define, Confluence */
define('conf-glossary/widget/highlight-dialog', ['require'], function(require) {
    var $ = require('jquery');
    var Backbone = require('backbone');
    var ScrollingInlineDialog = Confluence.ScrollingInlineDialog;
    var DocThemeUtils = Confluence.DocThemeUtils;
    var Meta = AJS.Meta;
    var Templates = Confluence.GlossaryOfTermsPlugin.Templates.Widget.HighlightDialog;

    var DIALOG_MAX_HEIGHT = 200;
    var DIALOG_WIDTH = 300;
    var highlightDemoDialog;
    var defaultDialogOptions = {
        hideDelay: null,
        width : DIALOG_WIDTH,
        maxHeight: DIALOG_MAX_HEIGHT
    };

    // TODO: This should be in a form utils module
    function serializeFormElement(formElement) {
        var o = {};
        var a = $(formElement).serializeArray();
        $.each(a, function() {
            if (o[this.name] !== undefined) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    }

    function getSpaceKey() {
        return Meta.get('space-key');
    }

    function closeFn() {
        highlightDemoDialog && highlightDemoDialog.remove();
    }

    function showHighlightDemoDialog(selectionObject) {
        closeFn();
        var displayFn = function(content, trigger, showPopup) {
            var html = Templates.createDialogContent({
                term: selectionObject.text,
                space: getSpaceKey(),
                foundNum: selectionObject.searchText.numMatches
            });
            var $content = $(content);
            $content.html(html);

            showPopup();

            // TODO: Better focus control.
            setTimeout(function() {
                $content.find("textarea").first().focus();
            }, 4);

            return false;
        };
        return highlightDemoDialog = _openDialog(selectionObject, 'define-term-dialog', defaultDialogOptions, displayFn);
    }

    function _openDialog(selectionObject, id, options, displayFn) {
        var $target = $("<div>");
        _appendDialogTarget(selectionObject.area.average, $target);
        var originalCallback = options.hideCallback;
        options.hideCallback = function() {
            $target.remove(); // clean up dialog target element when hiding the dialog
            originalCallback && originalCallback();
        };
        var dialog = ScrollingInlineDialog($target, id, displayFn, options);
        dialog.show();
        return dialog;
    }

    function _appendDialogTarget(targetDimensions, $target) {
        DocThemeUtils.appendAbsolutePositionedElement($target);
        $target.css({
            top: targetDimensions.top,
            height: targetDimensions.height,
            left: targetDimensions.left,
            width: targetDimensions.width,
            'z-index': -9999,
            position: 'absolute'
        });
    }

    return Backbone.View.extend({
        initialize: function() {
            this.handlers = [];
        },
        events: {
            "submit": "onSubmitWrapper"
        },
        onSubmitWrapper: function(e) {
            var form = e.target;
            var data = serializeFormElement(form);
            this.handlers.forEach(function(handler) {
                handler(data);
            });
            e.preventDefault();
            closeFn();
        },
        /**
         * Will register a handler function that will be
         * @param {Function} handlerFn - will be called when the form inside the dialog is submitted
         */
        onSubmit: function(handlerFn) {
            this.handlers.push(handlerFn);
        },
        render: function (obj) {
            this.setElement(showHighlightDemoDialog(obj));
            return this;
        },
        showHighlightDialog: function(obj) {
            return this.render(obj);
        }
    });
});
