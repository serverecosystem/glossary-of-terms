define('conf-glossary/feature/glossary-notifications/glossary-notifications', [
    'conf-glossary/feature/glossary/glossary',
    'aui/flag',
    'jquery'
], function(glossary, flag, $) {

    glossary.on('sync', function() {
        console.log('glossary was synced', arguments);
    });

    glossary.on('add', function(model) {
        console.log('term added', model);
    });

    glossary.on('persisted', function(response) {
        console.log('persisted', response);
        var f = flag({
            type: 'success',
            title: 'Term definition saved!',
            persistent: false,
            body: 'Thanks for making <conf-term>Confluence</conf-term> a more legible place! &#x1f64c;'
        });
        setTimeout(function() {
            f.close();
        }, 5000);
    });
});
