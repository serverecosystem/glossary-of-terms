/**
 * Finds defined terms in the current page, and replaces their first occurrence with
 * an element representing the actual definition for that term.
 *
 * This module would not need to exist if I knew how to:
 *
 * - plug in to the storage format's marshalling/unmarshalling process
 * - communicate with an external service (i.e., micros) from Confluence
 */
define('conf-glossary/feature/term-linker/term-linker', [
    'conf-glossary/feature/glossary/glossary',
    'conf-glossary/feature/term/element',
    'conf-glossary/lib/underscore',
    'jquery'
], function(glossary, TermElement, _, $) {

    function fuzzyIndexOf(haystack, needle, offset) {
        offset || (offset = 0);
        return haystack.toLowerCase().indexOf(needle.toLowerCase(), offset);
    }

    function getTextFromSomething(thing) {
        if (!thing) return "";
        else if (typeof thing === "string") return thing;
        else if (thing.jquery) return thing.text();
        else if (!thing.nodeType) return thing;
        else if (thing.nodeType == 3) return thing.textContent;
        else if (thing.nodeType == 1) return thing.innerHTML;
        else return "";
    }

    function hasMatch(haystack, needle, hardMode) {
        hardMode = !!hardMode;
        haystack = getTextFromSomething(haystack);
        if (!haystack.length) return false;
        if (typeof needle !== "string" || !needle.length) return false;
        var boundaryMatcher = /[\[\]\(\)\{\}\s\.,:;'"!?@#\$%\^&*\-\_+=\|\\\/\<\>]/; // Obviously incomplete, but should at least aid in finding utf8 words
        var match = -1, left, right;
        do {
            match = fuzzyIndexOf(haystack, needle, match+1);
            if (match == -1) return false;
            if (hardMode) {
                left = (match == 0) ? 0 : haystack.charAt(match-1).search(boundaryMatcher);
                right = (match+needle.length >= haystack.length) ? 0 : haystack.charAt(match+needle.length).search(boundaryMatcher);
            }
        } while (match > -1 && (left == -1 || right == -1));
        return match !== -1;
    }

    function getContentBlocks() {
        return $(".wiki-content");
    }

    function getDescendantTextNodes(node) {
        var nodes = [];
        if (node) {
            if (node.nodeType == 3) nodes.push(node);
            if (node.nodeType == 1) {
                _.each(node.childNodes, function(child) {
                    nodes.push(getDescendantTextNodes(child));
                });
            }
        }
        return _.flatten(nodes);
    }

    function replaceTextNodeWithTerm(node, termName) {
        var text = node.textContent;
        var idx = fuzzyIndexOf(text, termName);
        var parts = [text.substr(0,idx), text.substr(idx, termName.length), text.substr(idx+termName.length)];
        var $replacement = createTermElement(parts[1]);
        var before = document.createTextNode(parts[0]);
        node.parentNode.insertBefore(before, node);
        $replacement.insertBefore(node);
        node.textContent = parts[2] || "";
    }

    function createTermElement(termName) {
        var el = new TermElement();
        el.setAttribute('title', termName);
        return $(el);
    }

    function addTermsToPage(terms) {
        var contentBlocks = getContentBlocks();
        var order = _.sortBy(terms, function(a) {
            return 0 - a.get('name').length;
        });
        _.each(order, function(term) {
            var name = term.get('name');
            var replaced = false;

            if (hasMatch(contentBlocks, name)) {
                contentBlocks.each(function() {
                    if (replaced) return; // short-circuit
                    var textNodes = getDescendantTextNodes(this);
                    var firstNodeWithMatch = _.find(textNodes, function(node) {
                        return hasMatch(node, name, true);
                    });
                    if (firstNodeWithMatch) {
                        replaceTextNodeWithTerm(firstNodeWithMatch, name);
                        replaced = true;
                    }
                });
            }
        });
    }

    return {
        addTermsToPage: addTermsToPage
    }
});
