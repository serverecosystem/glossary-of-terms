define('conf-glossary/entity/term', [
    'conf-glossary/lib/backbone',
    'conf-glossary/lib/underscore',
    'jquery'
], function(Backbone, _, $) {
    return Backbone.Model.extend({
        idAttribute: 'slug',

        initialize: function(attrs, options) {
            // HACK: keep multiple definitions around. This'd be simpler if they were a proper collection.
            var self = this;
            self.on('change:definitions', function(model, defs) {
                var oldDefs = self.previous('definitions');
                self.attributes['definitions'] = _.union(oldDefs, defs); // TODO: not accurate, but it's shipit
            });
        },

        definitions: function getDefinitions() {
            return this.get('definitions');
        },

        sync: function(method, model) {
            var data = model.attributes;
            var slug = String(data.slug || data.name).toLowerCase();
            model.set('slug', slug);
        },

        domId: function shouldntBelongHereProbably() {
            return 'conf-glossary-term_' + this.cid;
        },

        parse: function parseData(data) {
            if (!data) return;

            if (data.definition) {
                data.definitions = [{
                    content: data.definition,
                    scope: data.scope
                }];
                delete data.definition;
                delete data.scope; // TODO 9: really need to make this data a proper collection
            }
            return data;
        }
    });
});
