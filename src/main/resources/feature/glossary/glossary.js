define('conf-glossary/feature/glossary/glossary', [
    'conf-glossary/entity/term',
    'conf-glossary/entity/glossary-collection',
    'conf-glossary/feature/library/local-library',
    'conf-glossary/feature/library/state-library'
], function(
    Term,
    GlossaryCollection,
    localLibrary,
    stateLibrary
) {
    'use strict';

    var OurTermModel = Term.extend({
        save: function() {
            Term.prototype.save.apply(this);
            var stateLibrarySyncResult = stateLibrary.save(this);
            return stateLibrarySyncResult;
        }
    });

    var OurClossaryCollection = GlossaryCollection.extend({
        // We extend the basic Term so that they can be saved locally.
        model: OurTermModel,

        // If we wanted to persist to a remote service, we'd configure it here.
        // Very likely we would pass in the URL via WRM.data.claim.
        url: '#',

        sync: function(method, collection, options) {
            options.dataType = 'jsonp';
            // TODO: Persist the terms to a remote server, if we so desire.
            // var remoteSyncResult = Backbone.sync(method, collection, options);
            var remoteSyncResult = new $.Deferred().resolve().promise();
            // Save the terms to a browser-based store, too.
            var stateLibrarySyncResult = stateLibrary.saveAll(collection.toJSON());
            // Let Backbone figure out when these operations are done.
            return $.when(remoteSyncResult, stateLibrarySyncResult);
        }
    });

    // TODO: These could be loaded via WRM.data.claim and/or the localLibrary
    var seedTerms = [];
    var ourGlossary = new OurClossaryCollection(seedTerms);

    function addTermsToOurGlossary(terms) {
        ourGlossary.add(terms);
    }

    localLibrary.load().then(addTermsToOurGlossary);
    stateLibrary.load().then(addTermsToOurGlossary);

    return ourGlossary;
});
